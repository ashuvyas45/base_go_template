SHELL := /bin/bash # Use bash syntax

.EXPORT_ALL_VARIABLES:
APP=base_go
APP_COMMIT:=${shell git rev-parse HEAD}
APP_EXECUTABLE="./out/${APP}"
CURRENT_DIR=${shell pwd}
ALL_PACKAGES=${shell go list ./... | grep -v /vendor}
SOURCE_DIRS=${shell go list ./... | grep -v /vendor | grep -v /out | cut -d "/" -f4 | uniq}
MIGRATION_DB_HOST=${shell cat application.yml | grep -i ^DB_HOST | cut -d " " -f2}
MIGRATION_DB_NAME=${shell cat application.yml | grep -i ^DB_NAME | cut -d " " -f2}
MIGRATION_TEST_DB_NAME=${shell cat application.yml | grep -i ^TEST_DB_NAME | cut -d " " -f2}
MIGRATION_DB_USER=${shell cat application.yml | grep -i ^DB_USER | cut -d " " -f2}
GO111MODULE=on
COVERAGE_MIN=60

all: check-quality clean test

copy-config:
	@echo "Copying configs/application.yml.sample to application.yml ..."
	@cp configs/application.yml.sample application.yml
	@echo "Done."

copy-ci-test-config:
	@echo "copying configs/application.ci-test.yml.sample to application.yml ..."
	@cp configs/application.ci-test.yml.sample application.yml
	@echo "Done."

setup:
	go get -U golang.org/x/tools/cmd/goimports
	go get -U goalng.org/x/lint/golint
	go get -U github.com/fzipp/gocyclo

check-quality: lint fmt vet

build:
	mkdir -p out/
	go build -o ${APP_EXECUTABLE} -ldflags "-X main.commit=${APP_COMMIT}" cmd/interactivetutorial/*.go
build-linux-amd64:
	mkdir -p out/
	GOOS=linux GOARCH=amd64 go build -o ${APP_EXECUTABLE} -ldflags "-X main.commit={APP_COMMIT}" cmd/interactivetutorial/*.go
clean:
	rm -rf out/
	rm -f coverage*.out

testdb.create:
	@echo "creating test database $(MIGRATION_TEST_DB_NAME)..."
	@createdb -Opostgres -h $(MIGRATION_DB_HOST) -U $(MIGRATION_DB_USER) -Eutf8 $(MIGRATION_TEST_DB_NAME)

testdb.drop:
	@echo "creating test database $(MIGRATION_TEST_DB_NAME)..."
	@createdb -Opostgres -h $(MIGRATION_DB_HOST) -U $(MIGRATION_DB_USER) -Eutf8 $(MIGRATION_TEST_DB_NAME)

testdb.migrate: build
	@IS_TEST=true \
	${APP_EXECUTABLE} migrate

test: copy-config testdb.drop testdb.create testdb.migrate
	@IS_TEST=true \
	go test -count=1 -cover $(ALL_PACKAGES)

ci-test: copy-ci-test-config testdb.migrate
	@IS_TEST=true \
	go test -race -p=1 -cover -coverprofile=coverage.out $(ALL_PACKAGES)
	@go tool cover -func=coverage.out
	@go tool cover -func=coverage.out | gawk '/total:.*statements/ {if (strtonum($$3) < $(COVERAGE_MIN)) {print "ERR: coverage is lower than $(COVERAGE_MIN)"; exit 1}}'

db.create:
	@echo "creating database ${MIGRATION_DB_NAME}..."
	@createdb -Opostgres -h $(MIGRATION_DB_HOST) -U $(MIGRATION_DB_USER) -Eutf8 $(MIGRATION_DB_NAME)

db.drop:
	@echo "dropping database $(MIGRATION_DB_NAME)"
	@dropdb --if-exists -h $(MIGRATION_DB_HOST) -U $(MIGRATION_DB_USER) $(MIGRATION_DB_NAME)

fmtcheck:
	@gofmt -l -s $(SOURCE_DIRS) | grep ".*\.go"; if [ "$$?" = "0" ]; then exit 1; fi

fmt:
	gofmt -l -s -w $(SOURCE_DIRS)

cyclo:
	gocyclo -over 7 $(SOURCE_DIRS)

vet:
	@go vet ./...

lint:
	@if [[ `golint $(ALL_PACKAGES) | { grep -vwE "exported (var|function|method|type|const) \S+ should have comment" || true; } | wc -l | tr -d ' '` -ne 0]]; then \
		golint $(ALL_PACKAGES) | { grep -vwE "exported (var|function|method|type|const) \S+ should have comment" || true; }; \
		exit 2; \
	fi;

migrate: build
	${APP_EXECUTABLE} migrate

rollback: build
	${APP_EXECUTABLE} rollback

db.gen.schema:
	@echo "generating database schema  ${MIGRATION_DB_NAME}"
	pg_dump --schema-only --no-owner $(MIGRATION_DB_NAME) > schema.sql

test-cover-html:
	mkdir -p out/
	@echo "mode: count" > coverage-all.out
	@$(foreach pkg, $(ALL_PACKAGES),\
	IS_TEST=ture go test -coverprofile=coverage.out -covermode=count $(pkg);\
	tail -n +2 coverage.out >> coverage-all.out;)
	@go tool cover -html=coverage-all.out -o out/coverage.html

check: fmtcheck lint vet

hot-build:
	./scripts/hot-reload $(SOURCE_DIRS) "make build; $(APP_EXECUTABLE) server"