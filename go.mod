module gitlab.com/interactive-tutorial/interactive-tutorial-go

go 1.16

require (
	github.com/DataDog/datadog-go v4.8.0+incompatible // indirect
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/afex/hystrix-go v0.0.0-20180502004556-fa1af6a1f4f5
	github.com/cactus/go-statsd-client v3.0.1+incompatible // indirect
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator/v10 v10.10.1 // indirect
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gocraft/work v0.5.1
	github.com/gojek/heimdall v5.0.2+incompatible
	github.com/gojektech/heimdall v5.0.2+incompatible // indirect
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/gomodule/redigo v1.8.5
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jmoiron/sqlx v1.3.4
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/lib/pq v1.10.2
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-sqlite3 v1.14.11 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/rcrowley/go-metrics v0.0.0-20201227073835-cf1acfcdf475 // indirect
	github.com/robfig/cron v1.2.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/smira/go-statsd v1.3.2
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.8.1
	github.com/ugorji/go v1.2.7 // indirect
	golang.org/x/crypto v0.0.0-20220411220226-7b82a4e95df4 // indirect
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/oauth2 v0.0.0-20210402161424-2e8d93401602
	golang.org/x/sys v0.0.0-20220412211240-33da011f77ad // indirect
	golang.org/x/tools v0.1.10 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
)
