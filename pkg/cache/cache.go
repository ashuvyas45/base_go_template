package cache

import (
	"errors"
	"time"

	"github.com/go-redis/redis"
)

type Client interface {
	redis.Cmdable
	Process(cmd redis.Cmder) error
	Close() error
}

type Options struct {
	Addrs                                               []string
	PoolSize                                            int
	DialTimeout, ReadTimeout, WriteTimeout, IdleTimeout time.Duration
}

func NewClient(opts Options) (Client, error) {
	if len(opts.Addrs) == 0 {
		return nil, errors.New("no address to connect to redis")
	}

	if len(opts.Addrs) > 1 {
		return redis.NewClusterClient(opts.cluster()), nil
	}

	r := redis.NewClient(opts.singleton())
	_, err := r.Ping().Result()
	return r, err

}

func (o Options) cluster() *redis.ClusterOptions {
	return &redis.ClusterOptions{
		Addrs:        o.Addrs,
		PoolSize:     o.PoolSize,
		DialTimeout:  o.DialTimeout,
		WriteTimeout: o.WriteTimeout,
		ReadTimeout:  o.ReadTimeout,
		IdleTimeout:  o.IdleTimeout,
	}
}

func (o Options) singleton() *redis.Options {
	var addr string

	if len(o.Addrs) > 0 {
		addr = o.Addrs[0]
	}

	return &redis.Options{
		Addr:         addr,
		PoolSize:     o.PoolSize,
		DialTimeout:  o.DialTimeout,
		WriteTimeout: o.WriteTimeout,
		ReadTimeout:  o.ReadTimeout,
		IdleTimeout:  o.IdleTimeout,
	}
}
