package errors

import "fmt"

const (
	InternalError  = "internal_server_error"
	BadRequest     = "bad_request_format"
	InvalidRequest = "invalid_request"
	NotFound       = "record_not_found"
)

// Error represent service errors. This capture extra information than
// golang error which is error code.
type Error interface {
	Error() string
	ErrorCode() string
}

// New create an error with given message
func New(msg string, args ...interface{}) error {
	return fmt.Errorf(msg, args...)
}

// GenericError should be used to represent service errors. Code can be used to
// represent different types of errors which can be used by clients to handle the
// errors appropriately. Message should contain a default user friendly message.
type GenericError struct {
	Message string `json:"message"`
	Code    string `json:"code"`
}

// Error return the error message
func (ed *GenericError) Error() string {
	return ed.Message
}

// ErrorCode return the error code
func (ed *GenericError) ErrorCode() string {
	return ed.Code
}

type BadRequestError struct {
	GenericError
}

func NewBadRequestError(message string) *BadRequestError {
	return &BadRequestError{GenericError{
		Message: message,
		Code:    BadRequest,
	}}
}

type ValidationError struct {
	Errors []GenericError
}

func NewValidationError() *ValidationError {
	return &ValidationError{Errors: []GenericError{}}
}

func (v *ValidationError) Add(message string) {
	v.Errors = append(v.Errors, GenericError{
		Message: message,
		Code:    InvalidRequest,
	})
}

func (v *ValidationError) ErrOrNil() error {
	if len(v.Errors) == 0 {
		return nil
	}

	errMsg := ""

	for _, e := range v.Errors {
		errMsg = errMsg + ", " + e.Message
	}

	return &GenericError{
		Message: errMsg,
		Code:    InvalidRequest,
	}
}

type UnknownError struct {
	GenericError
}

func NewUnknownError(message string) *UnknownError {
	return &UnknownError{GenericError{
		Message: message,
		Code:    InternalError,
	}}
}

type RecordNotFound struct {
	GenericError
}

func NewRecordNotFoundError(message string) *RecordNotFound {
	return &RecordNotFound{GenericError{
		Message: message,
		Code:    NotFound,
	}}
}
