package logger

import (
	"log"
	"net/http"
	"os"

	"github.com/sirupsen/logrus"
)

var logger *logrus.Logger

// Config represent configuration for the logger
type Config struct {
	Level  string
	Format string
}

func SetupLogger(config Config) {
	level, err := logrus.ParseLevel(config.Level)
	if err != nil {
		log.Fatalf(err.Error())
	}

	logger = &logrus.Logger{
		Out:   os.Stdout,
		Hooks: make(logrus.LevelHooks),
		Level: level,
	}

	if config.Format == "json" {
		logger.Formatter = &logrus.JSONFormatter{}
	} else {
		logger.Formatter = &logrus.TextFormatter{}
	}
}

// WithField captures additional information for the logger
func WithField(key string, value interface{}) *logrus.Entry {
	return logger.WithField(key, value)
}

// WithFields captures addition informations for the logger
func WithFields(fields map[string]interface{}) *logrus.Entry {
	return logger.WithFields(fields)
}

// WithRequest capture information from http request
func WithRequest(r *http.Request) *logrus.Entry {
	return logger.WithFields(logrus.Fields{
		"method": r.Method,
		"host":   r.Host,
		"path":   r.URL.Path,
	})
}

// Debug logs message at debug log level
func Debug(args ...interface{}) {
	logger.Debug(args...)
}

// Debugf logs formated message at debug log level
func Debugf(format string, args ...interface{}) {
	logger.Debugf(format, args...)
}

// Info logs message at Info log level
func Info(args ...interface{}) {
	logger.Info(args...)
}

// Infof logs formated message at Info log level
func Infof(format string, args ...interface{}) {
	logger.Infof(format, args...)
}

// Warn logs message at Warn log level
func Warn(args ...interface{}) {
	logger.Warn(args...)
}

// Warnf logs formated message at Warn log level
func Warnf(format string, args ...interface{}) {
	logger.Warnf(format, args...)
}

// Error logs message at Error log level
func Error(args ...interface{}) {
	logger.Error(args...)
}

// Errorf logs formated message at Error log level
func Errorf(format string, args ...interface{}) {
	logger.Errorf(format, args...)
}

// Fatalf logs formated message at Fatal log level
func Fatalf(format string, args ...interface{}) {
	logger.Fatalf(format, args...)
}

// AddHook adds a new hook to logger
func AddHook(hook logrus.Hook) {
	logger.Hooks.Add(hook)
}

// For making default logger, this makes testing easier
func init() {
	SetupLogger(Config{
		Level:  "info",
		Format: "text",
	})
}
