package httpclient

import (
	"net"
	"net/http"
	"time"

	hystrixClient "github.com/gojek/heimdall/hystrix"
)

type HystrixConfig struct {
	Timeout                  int
	MaxConcurrentRequests    int
	RequestVolumeThreshold   int
	SleepWindow              int
	ErrorPercentageThreshold int
}

type Client struct {
	hc *hystrixClient.Client
}

func New(service string, cfg HystrixConfig) *Client {
	idleConnTimeout := time.Duration(60) * time.Second
	timeout := time.Duration(cfg.Timeout) * time.Second

	client := http.Client{
		Timeout: timeout,
		Transport: &http.Transport{
			MaxIdleConns:        cfg.MaxConcurrentRequests,
			MaxIdleConnsPerHost: cfg.MaxConcurrentRequests,
			IdleConnTimeout:     idleConnTimeout,
			DialContext:         (&net.Dialer{Timeout: timeout}).DialContext,
		},
	}

	hClient := hystrixClient.NewClient(
		hystrixClient.WithCommandName(service),
		hystrixClient.WithHTTPTimeout(timeout),
		hystrixClient.WithHystrixTimeout(timeout),
		hystrixClient.WithSleepWindow(cfg.SleepWindow),
		hystrixClient.WithMaxConcurrentRequests(cfg.MaxConcurrentRequests),
		hystrixClient.WithRequestVolumeThreshold(cfg.RequestVolumeThreshold),
		hystrixClient.WithErrorPercentThreshold(cfg.ErrorPercentageThreshold),
		hystrixClient.WithHTTPClient(&client),
	)

	return &Client{hc: hClient}
}

func (c *Client) Do(r *http.Request) (*http.Response, error) {
	return c.hc.Do(r)
}
