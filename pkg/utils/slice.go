package utils

func Contains(collection []string, s string) bool {
	for _, val := range collection {
		if val == s {
			return true
		}
	}

	return false
}
