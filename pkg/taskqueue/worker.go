package taskqueue

import "github.com/gocraft/work"

type JobOptions struct {
	Priority       uint // Priority from 1 to 10000
	MaxFails       uint // 1: send straight to dead (unless SkipDead)
	SkipDead       bool // If true, don't send failed jobs to the dead queue when tetries are exhausted.
	MaxConcurrency uint // Max number of jobs to keep in flight (default is 0, meaning no max)
}

type Worker interface {
	HandleJob(name string, opt JobOptions, fn interface{})
	PeriodicJob(name string, scheduleSpec string)
	DeadJobsCount() (int64, error)
	Start()
	Stop()
}

type worker struct {
	pool   *work.WorkerPool
	client *work.Client
}

func NewWorker(config Config) Worker {
	redisPool := newRedisPool(config)
	return &worker{
		pool:   work.NewWorkerPool(struct{}{}, 10, config.NameSpace, redisPool),
		client: work.NewClient(config.NameSpace, redisPool),
	}
}

func (w *worker) HandleJob(name string, opt JobOptions, fn interface{}) {
	w.pool.JobWithOptions(name, work.JobOptions{
		Priority:       opt.Priority,
		MaxFails:       opt.MaxFails,
		MaxConcurrency: opt.MaxConcurrency,
		SkipDead:       opt.SkipDead,
	}, fn)
}

func (w *worker) PeriodicJob(name string, scheduleSpec string) {
	w.pool.PeriodicallyEnqueue(scheduleSpec, name)
}

func (w *worker) Start() {
	w.pool.Start()
}

func (w *worker) Stop() {
	w.pool.Stop()
}

func (w *worker) DeadJobsCount() (int64, error) {
	_, count, err := w.client.DeadJobs(1)
	return count, err
}
