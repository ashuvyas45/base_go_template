package taskqueue

import (
	"time"

	"github.com/gomodule/redigo/redis"
)

type Config struct {
	RedisAddress       string
	RedisIdleTimeout   time.Duration
	RedisMaxActiveConn int
	RedisMaxIdleConn   int
	NameSpace          string
}

func newRedisPool(config Config) *redis.Pool {
	return &redis.Pool{
		IdleTimeout: config.RedisIdleTimeout,
		MaxActive:   config.RedisMaxActiveConn,
		MaxIdle:     config.RedisMaxIdleConn,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", config.RedisAddress)
		},
	}
}
