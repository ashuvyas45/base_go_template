package taskqueue

import "github.com/gocraft/work"

type Enqueuer interface {
	Enqueue(jobName string, args map[string]interface{}) (*work.Job, error)
}

func NewEnqueuer(config Config) Enqueuer {
	redisPool := newRedisPool(config)
	return work.NewEnqueuer(config.NameSpace, redisPool)
}
