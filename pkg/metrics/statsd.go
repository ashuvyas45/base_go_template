package metrics

import (
	"fmt"
	"time"

	metricCollector "github.com/afex/hystrix-go/hystrix/metric_collector"
	"github.com/afex/hystrix-go/plugins"
	statsd "github.com/smira/go-statsd"
)

var statsD *statsd.Client

type StatsDConfig struct {
	Enabled       bool
	Host          string
	Port          int
	FlushInterval time.Duration
	AppName       string
}

func InitializeStatsDClient(config StatsDConfig) error {
	if !config.Enabled {
		return nil
	}

	addr := fmt.Sprintf("%s:%d", config.Host, config.Port)
	prefix := statsd.MetricPrefix(config.AppName + ".")
	flushPeriod := statsd.FlushInterval(config.FlushInterval)

	statsD = statsd.NewClient(addr, prefix, flushPeriod)

	hc := plugins.StatsdCollectorConfig{
		StatsdAddr: addr,
		Prefix:     "Some.hystrix",
	}

	collector, err := plugins.InitializeStatsdCollector(&hc)
	if err != nil {
		statsD.Close()
		return fmt.Errorf("error initiating the hystrix metric collector on statsD %+v", err)
	}

	metricCollector.Registry.Register(collector.NewStatsdCollector)

	return nil
}

func StatsDClient() *statsd.Client {
	return statsD
}

func CloseStatsDClient() {
	if statsD != nil {
		statsD.Close()
	}
}

var (
	StringTag = statsd.StringTag
)
