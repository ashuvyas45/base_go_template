package db

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/logger"

	_ "github.com/lib/pq"
)

var (
	defaultMaxIdleConns = 10
	defaultMaxOpenConns = 10
	defaultConnMaxTime  = 30 * time.Minute

	postgresDriver = "postgres"
)

type contextKey string

func (ckt contextKey) String() string {
	return string(ckt)
}

const dbTransactionContextKey = contextKey("tx-transaction")

type Config struct {
	URL             string
	MaxIdleConns    int
	MaxOpenConns    int
	ConnMaxLifeTime time.Duration
}

func (c Config) maxIdleConns() int {
	if c.MaxIdleConns == 0 {
		return defaultMaxIdleConns
	}

	return c.MaxIdleConns
}

func (c Config) maxOpenConns() int {
	if c.MaxOpenConns == 0 {
		return defaultMaxOpenConns
	}

	return c.MaxOpenConns
}

func (c Config) maxConnLife() time.Duration {
	if c.ConnMaxLifeTime == 0 {
		return defaultConnMaxTime
	}

	return c.maxConnLife()
}

// NewDB create a new postgres db handler
func NewDB(config Config) (*sqlx.DB, error) {
	var (
		err error
		db  *sqlx.DB
	)

	db, err = sqlx.Open(postgresDriver, config.URL)
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	db.SetMaxIdleConns(config.maxIdleConns())
	db.SetMaxOpenConns(config.maxOpenConns())
	db.SetConnMaxLifetime(config.maxConnLife())

	return db, err
}

// Transact create and execute a transaction. Also take care of rollback or commit transaction
func Transact(ctx context.Context, dbx *sqlx.DB, txFunc func(ctx context.Context, tx *sqlx.Tx) error) error {
	dbTx, inTx := TxOrDBFromContext(ctx, dbx)
	if inTx {
		return txFunc(ctx, dbTx.(*sqlx.Tx))
	}

	tx, err := dbx.BeginTxx(ctx, nil)
	if err != nil {
		return fmt.Errorf("unable to create db transaction: %v", err)
	}

	if err := txFunc(ctx, tx); err != nil {
		if err := tx.Rollback(); err != nil {
			logger.Errorf("error while db transaction rollback: %v", err)
		}
		return err
	}

	return tx.Commit()
}

func ContextWithTx(ctx context.Context, tx *sqlx.Tx) context.Context {
	return context.WithValue(ctx, dbTransactionContextKey, tx)
}

func TxOrDBFromContext(ctx context.Context, db *sqlx.DB) (DB, bool) {
	tx, ok := ctx.Value(dbTransactionContextKey).(*sqlx.Tx)
	if ok {
		return tx, true
	}

	return db, false
}
