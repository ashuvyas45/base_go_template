package db

import (
	"database/sql"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"

	"errors"
)

const defaultMigrationsPath = "file://./db/migration"

var ErrNoMigrations = errors.New("no migration")

// RunDatabaseMigrations run the migration from the last ran migration
func RunDatabaseMigrations(connectionURL, path string) error {
	m, err := createMigration(connectionURL, path)
	if err != nil {
		return err
	}

	err = m.Up()
	if err == migrate.ErrNoChange || err == nil {
		return nil
	}

	return err
}

// RollbackLatestsMigration rollback the latest migration
func RollbackLatestMigration(connectionURL, path string) error {
	m, err := createMigration(connectionURL, path)
	if err != nil {
		return err
	}

	err = m.Steps(-1)
	if err == migrate.ErrNoChange || err == nil {
		return nil
	}

	return err
}

func createMigration(connectionURL, path string) (*migrate.Migrate, error) {
	if path == "" {
		path = defaultMigrationsPath
	}

	db, err := sql.Open(postgresDriver, connectionURL)
	if err != nil {
		return nil, err
	}

	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return nil, err
	}

	m, err := migrate.NewWithDatabaseInstance(path, postgresDriver, driver)
	if err != nil {
		return nil, err
	}

	return m, nil
}
