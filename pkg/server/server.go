package server

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/logger"
)

// ShutdownTimeout for grceful shutdown
const ShutdownTimeout = 1 * time.Second

// ReadWriteTimeout specifies timeout for http server read and write
const ReadWriteTimeout = 10 * time.Second

// New initializes the server with all the routes
func New(router http.Handler) *Server {
	return &Server{
		apiServer: &http.Server{
			Handler:     router,
			ReadTimeout: ReadWriteTimeout,
			IdleTimeout: ReadWriteTimeout,
		},
	}
}

// Server is a wrapper around http.Server and provides
// Serve method with graceful-shutdown enabled
type Server struct {
	apiServer *http.Server
}

// Serve starts the server and blocks until any termination
// signals and performs graceful shutdown
func (s *Server) Serve(addr string) {
	s.apiServer.Addr = addr
	go s.listerAndServe()
	s.waitForShutDown()

}

func (s *Server) listerAndServe() {
	logger.Infof("starting API server in %s", s.apiServer.Addr)
	if err := s.apiServer.ListenAndServe(); err != http.ErrServerClosed {
		logger.Fatalf(err.Error())
	}
}

func (s *Server) waitForShutDown() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig,
		syscall.SIGINT,
		syscall.SIGTERM)
	<-sig
	logger.Infof("API server shutting down")
	ctx, cancel := context.WithTimeout(context.Background(), ShutdownTimeout)
	defer cancel()
	s.apiServer.Shutdown(ctx)
	logger.Infof("API server shutdown complete")
}
