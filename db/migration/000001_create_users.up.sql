CREATE TABLE "users"
(
    "id"                 bigserial PRIMARY KEY,
    "name"               varchar NOT NULL,
    "passions"           jsonb,
    "bio"                varchar,
    "job_title"          varchar,
    "company"            varchar,
    "city"               varchar,
    "email"              varchar,
    "phone_number"       varchar,
    "gender"             varchar,
    "sexual_orientation" varchar,
    "config"             jsonb
);

CREATE UNIQUE INDEX idx_address_email ON users (email);
CREATE UNIQUE INDEX idx_address_phone ON users (phone_number);