package main

import (
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/login"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/user"
	"net/http"

	"github.com/jmoiron/sqlx"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/handlers"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/cache"
)

func createHandler(dbx *sqlx.DB, cacheClient cache.Client) http.Handler {
	req := initializeServices(dbx, cacheClient)
	return handlers.NewRouter(req)
}

func initializeServices(dbx *sqlx.DB, cacheClient cache.Client) handlers.Request {
	userRepo := user.NewUserStore(dbx)

	userService := user.NewService(userRepo)
	loginService := login.NewService(login.NewFactory(), userService)

	return handlers.Request{
		UserService:  userService,
		LoginService: loginService,
	}
}
