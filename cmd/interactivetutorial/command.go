package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/config"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/cache"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/db"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/logger"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/server"
)

func newCLI() *cobra.Command {
	cli := &cobra.Command{
		Use:   "interactivetutorial",
		Short: "interactive tutorial server service",
		PersistentPreRun: func(_ *cobra.Command, _ []string) {
			config.Load()
			logger.SetupLogger(config.Log())
		},
	}

	cli.AddCommand(newServerCmd())
	cli.AddCommand(migrateCmd())

	return cli
}

func newServerCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "server",
		Short: "start HTTP API server",
		Run: func(_ *cobra.Command, _ []string) {
			dbx, err := db.NewDB(db.Config{
				URL:          config.DB().ConnectionURL(),
				MaxIdleConns: config.DB().MaxPool(),
				MaxOpenConns: config.DB().MaxPool(),
			})
			if err != nil {
				logger.Fatalf("unable to connecto to database: %v", err)
			}
			defer dbx.Close()

			logger.Info("connection to database is successful")

			cacheClient, err := cache.NewClient(config.Cache())
			if err != nil {
				logger.Fatalf("unable to connect to redis: %v", err)
			}
			defer cacheClient.Close()

			logger.Info("connection to Cache is successful")

			s := server.New(createHandler(dbx, cacheClient))

			s.Serve(config.Addr())
		},
	}
}

func migrateCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "migrate",
		Short: "run database migration",
		Run: func(_ *cobra.Command, _ []string) {
			if err := db.RunDatabaseMigrations(config.DB().ConnectionURL(), ""); err != nil {
				logger.Fatalf("unable to run db migration: %v", err)
			}
		},
	}
}
