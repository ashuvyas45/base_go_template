package config

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/viper"
)

func mustGetString(key string) string {
	mustHave(key)
	return viper.GetString(key)
}

func mustGetInt(key string) int {
	mustHave(key)
	v, err := strconv.Atoi(viper.GetString(key))
	if err != nil {
		panic(fmt.Sprintf("value of key: %s is not a valid integer", key))
	}
	return v
}

func mustGetBool(key string) bool {
	mustHave(key)
	return viper.GetBool(key)
}

func mustGetTimeInSec(key string) time.Duration {
	return time.Second * time.Duration(mustGetInt(key))
}

func mustGetTimeInMs(key string) time.Duration {
	return time.Millisecond * time.Duration(mustGetInt(key))
}

func mustGetStringArray(key string) []string {
	strs := strings.Split(mustGetString(key), ",")
	for i, str := range strs {
		strs[i] = strings.TrimSpace(str)
	}
	return strs
}

func mustGetIntArray(key string) []int {
	strs := mustGetStringArray(key)
	ints := make([]int, len(strs))

	for i, str := range strs {
		v, err := strconv.Atoi(str)
		if err != nil {
			panic(fmt.Sprintf("invalid integer found in key: %s", key))
		}
		ints[i] = v
	}
	return ints
}

func mustHave(key string) {
	if !viper.IsSet(key) {
		panic(fmt.Sprintf("value for the key: %s is not set", key))
	}
}
