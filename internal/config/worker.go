package config

import "gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/taskqueue"

func newWorkerConfig() taskqueue.Config {
	return taskqueue.Config{
		RedisAddress:       mustGetString("WORKER_HOST"),
		RedisIdleTimeout:   mustGetTimeInMs("WORKER_CONNECT_TIMEOUT_MS"),
		RedisMaxActiveConn: mustGetInt("WORKER_MAX_POOL_SIZE"),
		RedisMaxIdleConn:   mustGetInt("WORKER_IDLE_POOL_SIZE"),
		NameSpace:          mustGetString("WORKER_NAMESPACE"),
	}
}
