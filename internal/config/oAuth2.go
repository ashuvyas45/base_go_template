package config

import (
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

func newGoogleOAuth2Config() *oauth2.Config {
	return &oauth2.Config{
		ClientID:     mustGetString("GOOGLE_OAUTH2_CLIENT"),
		ClientSecret: mustGetString("GOOGLE_OAUTH2_SECRET"),
		Endpoint:     google.Endpoint,
		RedirectURL:  mustGetString("OAUTH_REDIRECT_URL"),
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"},
	}
}
