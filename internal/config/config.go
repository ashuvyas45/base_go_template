package config

import (
	"fmt"
	"golang.org/x/oauth2"

	"github.com/spf13/viper"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/cache"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/logger"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/metrics"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/taskqueue"
)

var appConfig config

// Load the config from environment and/or config file
func Load() error {
	viper.SetDefault("APP_PORT", "8080")
	viper.SetDefault("LOG_LEVEL", "error")
	viper.SetDefault("LOG_FORMAT", "json")
	viper.SetDefault("IS_TEST", "false")

	viper.AutomaticEnv()
	viper.SetConfigName("application")
	viper.AddConfigPath("./")
	viper.AddConfigPath("../")
	viper.AddConfigPath("../../")
	viper.ReadInConfig()

	appConfig = config{
		port:        mustGetInt("APP_PORT"),
		database:    newDatabaseConfig(mustGetBool("IS_TEST")),
		log:         logger.Config{Level: mustGetString("LOG_LEVEL"), Format: mustGetString("LOG_FORMAT")},
		cache:       newCacheConfig(),
		statsd:      newStatsDConfig(),
		worker:      newWorkerConfig(),
		googleOAuth: newGoogleOAuth2Config(),
	}

	return nil
}

type config struct {
	port        int
	database    DatabaseConfig
	log         logger.Config
	cache       cache.Options
	statsd      metrics.StatsDConfig
	worker      taskqueue.Config
	googleOAuth *oauth2.Config
}

func Log() logger.Config           { return appConfig.log }
func DB() DatabaseConfig           { return appConfig.database }
func Addr() string                 { return fmt.Sprintf(":%d", appConfig.port) }
func Cache() cache.Options         { return appConfig.cache }
func GoogleOAuth() *oauth2.Config  { return appConfig.googleOAuth }
func StatsD() metrics.StatsDConfig { return appConfig.statsd }
func Worker() taskqueue.Config     { return appConfig.worker }
