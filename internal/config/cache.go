package config

import "gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/cache"

func newCacheConfig() cache.Options {
	return cache.Options{
		Addrs:        mustGetStringArray("CACHE_HOST"),
		PoolSize:     mustGetInt("CACHE_MAX_POOL_SIZE"),
		DialTimeout:  mustGetTimeInMs("CACHE_CONNECT_TIMEOUT_MS"),
		ReadTimeout:  mustGetTimeInMs("CACHE_QUERY_TIMEOUT_MS"),
		WriteTimeout: mustGetTimeInMs("CACHE_QUERY_TIMEOUT_MS"),
		IdleTimeout:  mustGetTimeInMs("CACHE_MAX_IDLE_TIMEOUT_MS"),
	}
}
