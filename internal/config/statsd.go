package config

import (
	"time"

	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/metrics"
)

func newStatsDConfig() metrics.StatsDConfig {
	return metrics.StatsDConfig{
		AppName:       mustGetString("STATSD_APP_NAME"),
		Host:          mustGetString("STATSD_HOST"),
		FlushInterval: time.Second * time.Duration(mustGetInt("STATSD_FLUSH_PERIOD_IN_SECONDS")),
		Port:          mustGetInt("STATSD_PORT"),
		Enabled:       mustGetBool("STATSD_ENABLED"),
	}
}
