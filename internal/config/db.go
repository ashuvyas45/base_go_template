package config

import "fmt"

type DatabaseConfig struct {
	name        string
	host        string
	user        string
	password    string
	port        int
	maxPoolSize int

	slaveHost string
}

func newDatabaseConfig(isTest bool) DatabaseConfig {
	dbConfig := DatabaseConfig{
		name:        mustGetString("DB_NAME"),
		host:        mustGetString("DB_HOST"),
		user:        mustGetString("DB_USER"),
		password:    mustGetString("DB_PASSWORD"),
		port:        mustGetInt("DB_PORT"),
		maxPoolSize: mustGetInt("DB_MAX_POOL"),

		slaveHost: mustGetString("DB_SLAVE_HOST"),
	}

	if isTest {
		dbConfig.name = mustGetString("TEST_DB_NAME")
		dbConfig.password = mustGetString("TEST_DB_PASSWORD")
	}

	return dbConfig
}

// ConnectionString returns database connection string for the give config
func (d DatabaseConfig) ConnectionString() string {
	return fmt.Sprintf("dbname=%s user=%s password='%s' host=%s port=%d sslmode=disable", d.name, d.user, d.password, d.host, d.port)
}

func (d DatabaseConfig) ConnectionURL() string {
	return fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable&timezone=UTC", d.user, d.password, d.host, d.port, d.name)
}

func (d DatabaseConfig) MaxPool() int {
	return d.maxPoolSize
}

func (d DatabaseConfig) SlaveConnectionString() string {
	return fmt.Sprintf("dbname=%s user=%s password='%s' host=%s port=%d sslmode=disable", d.name, d.user, d.password, d.slaveHost, d.port)
}

func (d DatabaseConfig) SlaveConnectionURL() string {
	return fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable&timezone=UTC", d.user, d.password, d.slaveHost, d.port, d.name)
}
