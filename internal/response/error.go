package response

import "net/http"

func HandleError(w http.ResponseWriter, err error) {
	var statusCode int
	var errors []GenericError

	switch err.(type) {
	default:
		statusCode = http.StatusInternalServerError
		errors = []GenericError{
			{
				Code:    "INTERNAL_SERVER_ERROR",
				Message: err.Error(),
				//Message: "Something went wrong and we are working on it!",
			},
		}
	}

	WriteFailure(w, errors, statusCode)
}
