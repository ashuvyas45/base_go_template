package response

import (
	"encoding/json"
	"net/http"
)

type GenericResponse struct {
	Success bool        `json:"success"`
	Data    interface{} `json:"data"`
	Errors  []GenericError
}

type GenericError struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

// WriteSuccess write response with status ok
func WriteSuccess(w http.ResponseWriter, data interface{}) {
	res := GenericResponse{
		Success: true,
		Data:    data,
	}

	writeAnyResponse(w, res, http.StatusOK)
}

// WriteFailure write response with given status code and errors
func WriteFailure(w http.ResponseWriter, errors []GenericError, statusCode int) {
	res := GenericResponse{
		Success: false,
		Data:    nil,
		Errors:  errors,
	}

	writeAnyResponse(w, res, statusCode)
}

func writeAnyResponse(w http.ResponseWriter, data interface{}, statusCode int) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)

	res, err := json.Marshal(data)
	if err != nil {
		return err
	}

	if _, err = w.Write(res); err != nil {
		return err
	}

	return nil
}
