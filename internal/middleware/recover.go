package middleware

import (
	"net/http"
	"runtime/debug"

	"github.com/gorilla/mux"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/logger"
)

func Recover() mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				if err := recover(); err != nil {
					logger.WithRequest(r).Errorf("Recovered from panic: %+v", err)
					debug.PrintStack()
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
			}()
			next.ServeHTTP(w, r)
		})
	}
}
