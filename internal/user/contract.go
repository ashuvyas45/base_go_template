package user

type CreateUserRequest struct {
	Name              string    `json:"name"`
	Bio               string    `json:"bio"`
	JobTitle          string    `json:"job_title"`
	Company           string    `json:"company"`
	City              string    `json:"city"`
	Gender            string    `json:"gender"`
	SexualOrientation string    `json:"sexual_orientation"`
	Config            ReqConfig `json:"config"`
	Email             string    `json:"email"`
	PhoneNumber       string    `json:"phone_number"`
}

type ReqConfig struct {
	ShowAge      bool `json:"show_age"`
	ShowDistance bool `json:"show_distance"`
}

func (r *CreateUserRequest) BuildUser() *User {
	return &User{
		Name:              r.Name,
		Bio:               r.Bio,
		JobTitle:          r.JobTitle,
		Company:           r.Company,
		City:              r.City,
		Gender:            r.Gender,
		SexualOrientation: r.SexualOrientation,
		Config: Config{
			ShowAge:      r.Config.ShowAge,
			ShowDistance: r.Config.ShowDistance,
		},
		Email:       r.Email,
		PhoneNumber: r.PhoneNumber,
	}
}

type CreateUserResponse struct {
	ID                string `json:"id"`
	Name              string `json:"name"`
	Bio               string `json:"bio"`
	JobTitle          string `json:"job_title"`
	Company           string `json:"company"`
	City              string `json:"city"`
	Gender            string `json:"gender"`
	SexualOrientation string `json:"sexual_orientation"`
	Config            Config `json:"config"`
	Email             string `json:"email"`
	PhoneNumber       string `json:"phone_number"`
}

func (r *CreateUserResponse) BuildFromUser(user *User) {
	r.ID = user.ID
	r.Bio = user.Bio
	r.City = user.City
	r.Name = user.Name
	r.Gender = user.Gender
	r.SexualOrientation = user.SexualOrientation
	r.Company = user.Company
	r.JobTitle = user.JobTitle
	r.Config = Config{
		ShowAge:      r.Config.ShowAge,
		ShowDistance: r.Config.ShowDistance,
	}
	r.Email = user.Email
	r.PhoneNumber = user.PhoneNumber
}

type GetAllUserResponse struct {
	Users []CreateUserResponse `json:"users"`
}

func (r *GetAllUserResponse) BuildFromUsers(users []User) {
	r.Users = []CreateUserResponse{}

	for _, u := range users {
		var userResp CreateUserResponse

		userResp.BuildFromUser(&u)
		r.Users = append(r.Users, userResp)
	}
}
