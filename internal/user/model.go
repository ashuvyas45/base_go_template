package user

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
)

type Passions []string

type User struct {
	ID                string
	Name              string
	Passions          Passions
	Bio               string
	JobTitle          string
	Company           string
	City              string
	Gender            string
	SexualOrientation string
	Config            Config
	Email             string
	PhoneNumber       string
}

type Config struct {
	ShowAge      bool
	ShowDistance bool
}

func (p Passions) Value() (driver.Value, error) {
	return json.Marshal(p)
}

func (p *Passions) Scan(value interface{}) error {
	b, ok := value.([]byte)
	if !ok {
		return errors.New("type assertion to []byte failed")
	}

	return json.Unmarshal(b, &p)
}

func (c Config) Value() (driver.Value, error) {
	return json.Marshal(c)
}

func (c *Config) Scan(value interface{}) error {
	b, ok := value.([]byte)
	if !ok {
		return errors.New("type assertion to []byte failed")
	}

	return json.Unmarshal(b, &c)
}
