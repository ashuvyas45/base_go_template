package user

import "context"

type Repository interface {
	Create(ctx context.Context, user *User) (*User, error)
	GetAll(ctx context.Context) ([]User, error)
	FindByEmailID(ctx context.Context, email string) (*User, error)
}
