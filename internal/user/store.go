package user

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/errors"
)

const (
	insertQuery = `INSERT INTO USERS (name, passions, bio, job_title, company, city, gender, sexual_orientation, config, email, phone_number) Values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) returning id;`
	getAllQuery = `SELECT id, name, passions, bio, job_title, company, city, gender, sexual_orientation, config, email, phone_number FROM users;`
	findByEmail = `SELECT id, name, passions, bio, job_title, company, city, gender, sexual_orientation, config, email, phone_number FROM users Where email=$1;`
)

type userStore struct {
	db *sqlx.DB
}

type Row interface {
	Scan(dest ...interface{}) error
}

func NewUserStore(db *sqlx.DB) *userStore {
	return &userStore{
		db: db,
	}
}

func (us *userStore) Create(ctx context.Context, user *User) (*User, error) {
	var id string

	row := us.db.QueryRowxContext(ctx, insertQuery,
		user.Name,
		user.Passions,
		user.Bio,
		user.JobTitle,
		user.Company,
		user.City,
		user.Gender,
		user.SexualOrientation,
		user.Config,
		user.Email,
		user.PhoneNumber,
	)

	if row.Err() != nil {
		return &User{}, row.Err()
	}

	if err := row.Scan(&id); err != nil {
		return &User{}, err
	}

	user.ID = id

	return user, nil
}

func (us *userStore) GetAll(ctx context.Context) ([]User, error) {
	var users []User

	rows, err := us.db.QueryxContext(ctx, getAllQuery)
	if err != nil {
		return users, err
	}

	if rows.Err() != nil {
		return users, rows.Err()
	}

	if err = scanUsers(rows, &users); err != nil {
		return users, err
	}

	if err = rows.Close(); err != nil {
		return users, err
	}

	return users, nil
}

func (us *userStore) FindByEmailID(ctx context.Context, email string) (*User, error) {
	u := User{}

	row := us.db.QueryRowxContext(ctx, findByEmail, email)
	if row.Err() != nil {
		return &u, row.Err()
	}

	user, err := scanUser(row)
	if err != nil {
		if err == sql.ErrNoRows {
			return &u, errors.NewRecordNotFoundError(fmt.Sprintf("not able to find user with email %s", email))
		}
		return &user, err
	}

	return &user, nil

}

func scanUsers(rows *sqlx.Rows, users *[]User) error {
	var err error

	for rows.Next() {
		user, err2 := scanUser(rows)
		if err2 != nil {
			err = err2
			break
		}

		*users = append(*users, user)
	}

	return err
}

func scanUser(row Row) (User, error) {
	var err error
	var user User

	err = row.Scan(
		&user.ID,
		&user.Name,
		&user.Passions,
		&user.Bio,
		&user.JobTitle,
		&user.Company,
		&user.City,
		&user.Gender,
		&user.SexualOrientation,
		&user.Config,
		&user.Email,
		&user.PhoneNumber,
	)

	if err != nil {
		return user, err
	}

	return user, nil
}
