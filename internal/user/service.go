package user

import (
	"context"
)

type Service struct {
	userRepo Repository
}

func NewService(repo Repository) *Service {
	return &Service{userRepo: repo}
}

func (s *Service) Create(ctx context.Context, req CreateUserRequest) (*User, error) {
	userQuery := req.BuildUser()

	user, err := s.userRepo.Create(ctx, userQuery)
	if err != nil {
		return userQuery, err
	}
	return user, nil
}

func (s *Service) GetAll(ctx context.Context) ([]User, error) {
	var users []User

	users, err := s.userRepo.GetAll(ctx)
	if err != nil {
		return users, err
	}

	return users, nil
}

func (s *Service) FindByEmailID(ctx context.Context, email string) (*User, error) {
	user, err := s.userRepo.FindByEmailID(ctx, email)
	if err != nil {
		return user, err
	}

	return user, err
}
