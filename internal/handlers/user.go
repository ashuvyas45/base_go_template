package handlers

import (
	"context"
	"encoding/json"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/response"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/user"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/errors"
	"net/http"
)

type UserService interface {
	Create(ctx context.Context, req user.CreateUserRequest) (*user.User, error)
	GetAll(ctx context.Context) ([]user.User, error)
}

type UserHandler struct {
	service UserService
}

func NewUserHandler(service UserService) *UserHandler {
	return &UserHandler{service: service}
}

func (h *UserHandler) Create(w http.ResponseWriter, r *http.Request) {
	var req user.CreateUserRequest

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		newErr := errors.NewBadRequestError(err.Error())
		response.HandleError(w, newErr)
		return
	}

	u, err := h.service.Create(r.Context(), req)
	if err != nil {
		response.HandleError(w, err)
		return
	}

	var res user.CreateUserResponse
	res.BuildFromUser(u)

	response.WriteSuccess(w, res)
}

func (h *UserHandler) FindAll(w http.ResponseWriter, r *http.Request) {
	u, err := h.service.GetAll(r.Context())
	if err != nil {
		response.HandleError(w, err)
		return
	}

	var res user.GetAllUserResponse
	res.BuildFromUsers(u)

	response.WriteSuccess(w, res)
}
