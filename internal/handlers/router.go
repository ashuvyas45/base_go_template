package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Request struct {
	UserService  UserService
	LoginService LoginService
}

func NewRouter(req Request) http.Handler {
	router := gin.Default()

	addUserRoutes(router, req)
	addLoginRouter(router, req)

	return router
}

func addUserRoutes(router gin.IRoutes, request Request) {
	userHandler := NewUserHandler(request.UserService)

	router.POST("/users", ginWrapper(userHandler.Create))
	router.GET("/users",ginWrapper(userHandler.FindAll))
}

func addLoginRouter(router gin.IRoutes, request Request) {
	loginHandler := NewLoginHandler(request.LoginService)

	router.GET("/login", ginWrapper(loginHandler.Login))
	router.GET("/verify", ginWrapper(loginHandler.Verify))
}

func ginWrapper(handler http.HandlerFunc) gin.HandlerFunc {
	return func(c *gin.Context) {
		handler(c.Writer, c.Request)
	}
}