package handlers

import (
	"context"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/login"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/response"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/user"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/logger"
	"net/http"
	"time"
)

const (
	CookieExpiryTimeInHours = 5
)

type LoginService interface {
	Login(ctx context.Context, req login.Request) (login.Response, error)
	GetUserData(ctx context.Context, req login.VerifyRequest) (*user.User, error)
}

type LoginHandler struct {
	service LoginService
}

func NewLoginHandler(service LoginService) *LoginHandler {
	return &LoginHandler{
		service: service,
	}
}

func (l *LoginHandler) Login(w http.ResponseWriter, r *http.Request) {
	var req login.Request
	req.Mode = r.FormValue("mode")

	if err := req.Validate(); err != nil {
		response.HandleError(w, err)
		return
	}

	res, err := l.service.Login(r.Context(), req)

	if err != nil {
		response.HandleError(w, err)
		return
	}

	expiration := time.Now().Add(CookieExpiryTimeInHours * time.Hour)
	cookie := http.Cookie{Name: "oauthstate", Value: res.Token, Expires: expiration}
	http.SetCookie(w, &cookie)

	http.Redirect(w, r, res.URL, http.StatusTemporaryRedirect)
}

func (l *LoginHandler) Verify(w http.ResponseWriter, r *http.Request) {
	oauthState, _ := r.Cookie("oauthstate")

	if r.FormValue("state") != oauthState.Value {
		logger.Error("oAuth State is invalid")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	data, err := l.service.GetUserData(r.Context(), login.VerifyRequest{
		Token: r.FormValue("code"),
		Mode:  login.Google,
	})
	if err != nil {
		logger.Error(err.Error())
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	response.WriteSuccess(w, data)
}
