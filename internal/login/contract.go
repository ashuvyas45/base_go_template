package login

import (
	"fmt"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/errors"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/utils"
)

var allowedMode = []string{"google"}

type Request struct {
	Mode string `json:"mode"`
}

func (r *Request) Validate() error {
	vErr := errors.NewValidationError()

	if !utils.Contains(allowedMode, r.Mode) {
		vErr.Add(fmt.Sprintf("%s is not a allowd login mode", r.Mode))
	}

	return vErr.ErrOrNil()
}

type Response struct {
	URL   string
	Token string
}

type VerifyRequest struct {
	Token string
	Mode  string
}
