package login

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/config"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/user"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/errors"
	"io/ioutil"
	"math/rand"
	"net/http"
)

const oauthGoogleURLAPI = "https://www.googleapis.com/oauth2/v2/userinfo?access_token="

type userData struct {
	ID            string `json:"id"`
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email"`
	Name          string `json:"name"`
}

type GoogleService struct {
}

func NewGoogleLoginService() *GoogleService {
	return &GoogleService{}
}

func (g *GoogleService) URL() Response {
	oauthState := generateStateOauthCookie()

	u := config.GoogleOAuth().AuthCodeURL(oauthState)

	return Response{
		URL:   u,
		Token: oauthState,
	}
}

func (g *GoogleService) GetUserData(ctx context.Context, token string) (user.CreateUserRequest, error) {
	bytesData, err := getUserData(token)
	if err != nil {
		return user.CreateUserRequest{}, err
	}

	userFromToken := userData{}
	if err = json.Unmarshal(bytesData, &userFromToken); err != nil {
		return user.CreateUserRequest{}, err
	}

	userReq := user.CreateUserRequest{
		Name:              userFromToken.Name,
		Bio:               "",
		JobTitle:          "",
		Company:           "",
		City:              "",
		Gender:            "",
		SexualOrientation: "",
		Config:            user.ReqConfig{},
		Email:             userFromToken.Email,
	}

	return userReq, nil
}

func generateStateOauthCookie() string {
	b := make([]byte, 16)
	rand.Read(b)
	state := base64.URLEncoding.EncodeToString(b)

	return state
}

func getUserData(code string) ([]byte, error) {
	googleOauthConfig := config.GoogleOAuth()

	token, err := googleOauthConfig.Exchange(context.Background(), code)
	if err != nil {
		return nil, errors.NewUnknownError(err.Error())
	}

	res, err := http.Get(oauthGoogleURLAPI + token.AccessToken)
	if err != nil {
		return nil, errors.NewUnknownError("failed to get user info")
	}

	defer res.Body.Close()

	contents, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.NewUnknownError("failed read response user data from token")
	}

	return contents, nil
}
