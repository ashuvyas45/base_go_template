package login

import (
	"context"
	nativeErr "errors"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/user"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/errors"
)

var recordNotFoundType *errors.RecordNotFound

type UserService interface {
	Create(ctx context.Context, req user.CreateUserRequest) (*user.User, error)
	FindByEmailID(ctx context.Context, email string) (*user.User, error)
}

type service struct {
	factory     Factory
	userService UserService
}

func NewService(factory Factory, us UserService) *service {
	return &service{
		factory:     factory,
		userService: us,
	}
}

func (s *service) Login(ctx context.Context, req Request) (Response, error) {
	ser, err := s.factory.GetLoginService(req.Mode)

	if err != nil {
		return Response{}, err
	}

	return ser.URL(), nil
}

func (s *service) GetUserData(ctx context.Context, req VerifyRequest) (*user.User, error) {
	ser, err := s.factory.GetLoginService(req.Mode)

	if err != nil {
		return &user.User{}, err
	}

	createUserReq, err := ser.GetUserData(ctx, req.Token)
	if err != nil {
		return &user.User{}, err
	}

	return findOrCreateUser(ctx, err, s, createUserReq)
}

func findOrCreateUser(ctx context.Context, err error, s *service, createUserReq user.CreateUserRequest) (*user.User, error) {
	u, err := s.userService.FindByEmailID(ctx, createUserReq.Email)
	if err != nil {
		if nativeErr.As(err, &recordNotFoundType) {
			nu, err2 := s.userService.Create(ctx, createUserReq)
			if err2 != nil {
				return nu, err2
			}
			return nu, nil
		}
		return u, err
	}

	return u, nil
}
