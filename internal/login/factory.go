package login

import (
	"context"
	"fmt"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/internal/user"
	"gitlab.com/interactive-tutorial/interactive-tutorial-go/pkg/errors"
)

const (
	Google = "google"
)

type Service interface {
	URL() Response
	GetUserData(ctx context.Context, token string) (user.CreateUserRequest, error)
}

type Factory struct {
}

func NewFactory() Factory {
	return Factory{}
}

func (f *Factory) GetLoginService(mode string) (Service, error) {
	switch mode {
	case Google:
		return NewGoogleLoginService(), nil
	default:
		return nil, errors.NewUnknownError(fmt.Sprintf("%s is not a valid mode", mode))
	}
}
